package uk.co.cablepost.extractinatortweaks.screen;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import uk.co.cablepost.extractinatortweaks.ExtractinatorTweaks;
import uk.co.cablepost.extractinatortweaks.block.TweakedExtractinatorBlockEntity;

public class ExtractinatorScreen extends HandledScreen<ExtractinatorScreenHandler> {
    private static final Identifier TEXTURE = new Identifier(ExtractinatorTweaks.MOD_ID, "textures/gui/container/extractinator.png");

    public ExtractinatorScreen(ExtractinatorScreenHandler handler, PlayerInventory inventory, Text title) {
        super(handler, inventory, title);
        backgroundHeight = 190;
    }

    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
        titleY -= 1;
        playerInventoryTitleY += 25;
    }

    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);

        float cooldownProgress = handler.getCooldownProgress();
        drawTexture(matrices, x + 85, y + 33, 176, 33, 80, (int)Math.floor(6 * cooldownProgress));
    }

    protected void drawForeground(MatrixStack matrices, int mouseX, int mouseY) {
        super.drawForeground(matrices, mouseX, mouseY);

        int status = handler.getStatus();

        if(status == TweakedExtractinatorBlockEntity.STATUS_BLOCKED_INVALID_INPUT || status == TweakedExtractinatorBlockEntity.STATUS_BLOCKED_INVALID_INPUT_ABOVE){
            int x = (backgroundWidth - 80) / 2;
            int y = (height - backgroundHeight) / 2;
            int xo = 0;
            int yo = -200;

            RenderSystem.setShader(GameRenderer::getPositionTexShader);
            RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
            RenderSystem.setShaderTexture(0, TEXTURE);

            drawTexture(matrices, x + xo, y + yo, 176, 50, 80, 32);

            String message = "Error";
            String message2 = "";

            if(status == TweakedExtractinatorBlockEntity.STATUS_BLOCKED_INVALID_INPUT){
                message = "Invalid input";
            }
            if(status == TweakedExtractinatorBlockEntity.STATUS_BLOCKED_INVALID_INPUT_ABOVE) {
                message = "Invalid block";
                message2 = "above";
            }

            float tx = (backgroundWidth - textRenderer.getWidth(message)) / 2f;
            float tx2 = (backgroundWidth - textRenderer.getWidth(message2)) / 2f;
            this.textRenderer.draw(matrices, message, tx + xo, (float)y + yo + 7, 0);
            this.textRenderer.draw(matrices, message2, tx2 + xo, (float)y + yo + 17, 0);
        }
    }

    @Override

    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        super.render(matrices, mouseX, mouseY, delta);
        this.drawMouseoverTooltip(matrices, mouseX, mouseY);
    }
}
