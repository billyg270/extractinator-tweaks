package uk.co.cablepost.extractinatortweaks.screen;

import com.github.alexnijjar.the_extractinator.TheExtractinator;
import com.github.alexnijjar.the_extractinator.registry.ModBlocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.slot.Slot;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.extractinatortweaks.ExtractinatorTweaks;
import uk.co.cablepost.extractinatortweaks.block.TweakedExtractinatorBlockEntity;

public class ExtractinatorScreenHandler extends ScreenHandler {
    public Inventory inventory;
    public PropertyDelegate propertyDelegate;

    public ExtractinatorScreenHandler(int syncId, PlayerInventory playerInventory) {
        this(syncId, playerInventory, new SimpleInventory(TweakedExtractinatorBlockEntity.INVENTORY_SIZE), new ArrayPropertyDelegate(TweakedExtractinatorBlockEntity.PROPERTY_DELEGATE_SIZE));
    }

    public ExtractinatorScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
        super(ExtractinatorTweaks.EXTRACTINATOR_SCREEN_HANDLER, syncId);

        this.inventory = inventory;
        this.propertyDelegate = propertyDelegate;
        this.addProperties(this.propertyDelegate);

        inventory.onOpen(playerInventory.player);

        this.addSlot(new Slot(inventory, 0, 80, 15));

        for (int m = 0; m < 3; ++m) {
            for (int l = 0; l < 9; ++l) {
                int slotNo = (l + m * 9) + 1;
                if(slotNo < TweakedExtractinatorBlockEntity.INVENTORY_SIZE) {
                    this.addSlot(new Slot(inventory, slotNo, 8 + l * 18, 41 + m * 18));
                }
            }
        }

        //The player inventory
        for (int m = 0; m < 3; ++m) {
            for (int l = 0; l < 9; ++l) {
                this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 108 + m * 18));
            }
        }

        //The player Hotbar
        for (int m = 0; m < 9; ++m) {
            this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 166));
        }
    }

    @Override
    public boolean canUse(PlayerEntity player) {
        return this.inventory.canPlayerUse(player);
    }

    @Override
    public ItemStack transferSlot(PlayerEntity player, int invSlot) {
        ItemStack newStack = ItemStack.EMPTY;
        Slot slot = this.slots.get(invSlot);
        if (slot.hasStack()) {
            ItemStack originalStack = slot.getStack();
            newStack = originalStack.copy();
            if (invSlot < this.inventory.size()) {
                if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.insertItem(originalStack, 0, this.inventory.size(), false)) {
                return ItemStack.EMPTY;
            }

            if (originalStack.isEmpty()) {
                slot.setStack(ItemStack.EMPTY);
            } else {
                slot.markDirty();
            }
        }

        return newStack;
    }

    public float getCooldownProgress(){
        int cooldown = this.propertyDelegate.get(TweakedExtractinatorBlockEntity.PROPERTY_DELEGATE_TRANSFER_COOLDOWN);
        if(cooldown < 0){
            cooldown = 0;
            //return 0;//Make arrow look empty
        }

        return 1f - ((float)cooldown / (float)TheExtractinator.CONFIG.extractinatorConfig.inputCooldown);
    }

    public int getStatus(){
        return this.propertyDelegate.get(TweakedExtractinatorBlockEntity.PROPERTY_DELEGATE_STATUS);
    }
}
