package uk.co.cablepost.extractinatortweaks;

import com.github.alexnijjar.the_extractinator.util.ModIdentifier;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.registry.Registry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.cablepost.extractinatortweaks.block.TweakedExtractinatorBlock;
import uk.co.cablepost.extractinatortweaks.block.TweakedExtractinatorBlockEntity;
import uk.co.cablepost.extractinatortweaks.screen.ExtractinatorScreenHandler;

public class ExtractinatorTweaks implements ModInitializer {

    public static String MOD_ID = "extractinatortweaks";

    public static final Logger LOGGER = LoggerFactory.getLogger(MOD_ID);

    public static TweakedExtractinatorBlock EXTRACTINATOR_BLOCK  = new TweakedExtractinatorBlock(FabricBlockSettings.copy(Blocks.IRON_BLOCK).sounds(BlockSoundGroup.NETHERITE).requiresTool());
    public static BlockEntityType<TweakedExtractinatorBlockEntity> EXTRACTINATOR_BLOCK_ENTITY;

    public static final ScreenHandlerType<ExtractinatorScreenHandler> EXTRACTINATOR_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(new ModIdentifier("extractinator_block"), ExtractinatorScreenHandler::new);

    @Override
    public void onInitialize() {
        ExtractinatorTweaks.EXTRACTINATOR_BLOCK_ENTITY = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                new ModIdentifier("extractinator_entity"),
                FabricBlockEntityTypeBuilder.create(TweakedExtractinatorBlockEntity::new, ExtractinatorTweaks.EXTRACTINATOR_BLOCK).build(null)
        );
    }
}