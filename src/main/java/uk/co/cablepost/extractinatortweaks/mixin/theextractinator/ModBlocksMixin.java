package uk.co.cablepost.extractinatortweaks.mixin.theextractinator;

import com.github.alexnijjar.the_extractinator.blocks.ExtractinatorBlock;
import com.github.alexnijjar.the_extractinator.registry.ModBlocks;
import com.github.alexnijjar.the_extractinator.util.ModIdentifier;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.registry.Registry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.extractinatortweaks.ExtractinatorTweaks;
import uk.co.cablepost.extractinatortweaks.block.TweakedExtractinatorBlockEntity;

@Mixin(ModBlocks.class)
public class ModBlocksMixin {
    @Inject(method = "register()V", at = @At("HEAD"), cancellable = true, remap = false)
    private static void injected(CallbackInfo ci) {
        Registry.register(Registry.BLOCK, new ModIdentifier("extractinator_block"), ExtractinatorTweaks.EXTRACTINATOR_BLOCK);
        Registry.register(Registry.BLOCK, new ModIdentifier("silt"), ModBlocks.SILT);
        Registry.register(Registry.BLOCK, new ModIdentifier("slush"), ModBlocks.SLUSH);

        Registry.register(Registry.BLOCK, new ModIdentifier("old_extractinator_block"), ModBlocks.EXTRACTINATOR_BLOCK);

        ci.cancel();
    }
}
