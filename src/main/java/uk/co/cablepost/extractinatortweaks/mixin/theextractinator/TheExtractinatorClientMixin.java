package uk.co.cablepost.extractinatortweaks.mixin.theextractinator;

import com.github.alexnijjar.the_extractinator.client.TheExtractinatorClient;
import com.github.alexnijjar.the_extractinator.client.renderer.ExtractinatorBlockEntityRenderer;
import com.github.alexnijjar.the_extractinator.client.renderer.ExtractinatorItemRenderer;
import com.github.alexnijjar.the_extractinator.networking.ModS2CPackets;
import com.github.alexnijjar.the_extractinator.registry.ModBlockEntities;
import com.github.alexnijjar.the_extractinator.registry.ModItems;
import net.fabricmc.fabric.api.client.model.ModelLoadingRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.BuiltinItemRendererRegistry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.extractinatortweaks.ExtractinatorTweaks;
import uk.co.cablepost.extractinatortweaks.TweakedExtractinatorBlockEntityRenderer;

@Mixin(TheExtractinatorClient.class)
public class TheExtractinatorClientMixin {
    @Inject(method = "onInitializeClient()V", at = @At("HEAD"), cancellable = true, remap = false)
    public void onInitializeClient(CallbackInfo ci) {
        BlockEntityRendererRegistry.register(ExtractinatorTweaks.EXTRACTINATOR_BLOCK_ENTITY, (ctx) -> new TweakedExtractinatorBlockEntityRenderer());

        ModelLoadingRegistry.INSTANCE.registerModelProvider((manager, out) -> {
            out.accept(TheExtractinatorClient.GRINDER_PATH);
        });

        ModelLoadingRegistry.INSTANCE.registerModelProvider((manager, out) -> {
            out.accept(TheExtractinatorClient.EXTRACTINATOR_BLOCK_PATH);
        });

        BuiltinItemRendererRegistry.INSTANCE.register(ModItems.extractinatorItem, new ExtractinatorItemRenderer());

        ModS2CPackets.Register();

        ci.cancel();
    }
}
