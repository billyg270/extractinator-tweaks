package uk.co.cablepost.extractinatortweaks.mixin.theextractinator;

import com.github.alexnijjar.the_extractinator.blocks.entity.ExtractinatorBlockEntity;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ExtractinatorBlockEntity.class)
public class ExtractinatorBlockEntityMixin {
    /*
    @ModifyVariable(method = "serverTick(Lnet/minecraft/world/World;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;Lcom/github/alexnijjar/the_extractinator/blocks/entity/ExtractinatorBlockEntity;)V", at = @At("STORE"), ordinal = 1)
    private static boolean injected(boolean dropBlocks) {
        return false;
    }
     */
}
