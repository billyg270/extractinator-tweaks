package uk.co.cablepost.extractinatortweaks.mixin.theextractinator;

import com.github.alexnijjar.the_extractinator.blocks.ExtractinatorBlock;
import com.github.alexnijjar.the_extractinator.blocks.entity.ExtractinatorBlockEntity;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ExtractinatorBlock.class)
public class ExtractinatorBlockMixin {
    @Inject(method = "createBlockEntity(Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/BlockState;)Lnet/minecraft/block/entity/BlockEntity;", at = @At("HEAD"), cancellable = true)
    public void inject(BlockPos pos, BlockState state, CallbackInfoReturnable<BlockEntity> cir){
        cir.setReturnValue(null);
    }

    @Inject(method = "getTicker(Lnet/minecraft/world/World;Lnet/minecraft/block/BlockState;Lnet/minecraft/block/entity/BlockEntityType;)Lnet/minecraft/block/entity/BlockEntityTicker;", at = @At("HEAD"), cancellable = true)
    public void inject(World world, BlockState state, BlockEntityType<?> type, CallbackInfoReturnable<BlockEntityTicker<?>> cir){
        cir.setReturnValue(null);
    }
}
