package uk.co.cablepost.extractinatortweaks.mixin.theextractinator;


import com.github.alexnijjar.the_extractinator.client.TheExtractinatorClient;
import com.github.alexnijjar.the_extractinator.compat.rei.ExtractinatorCategory;
import com.github.alexnijjar.the_extractinator.compat.rei.ExtractinatorDisplay;
import com.github.alexnijjar.the_extractinator.compat.rei.TheExtractinatorClientPlugin;
import com.github.alexnijjar.the_extractinator.data.SupportedBlock;
import com.github.alexnijjar.the_extractinator.util.ModIdentifier;
import com.github.alexnijjar.the_extractinator.util.ModUtils;
import me.shedaniel.rei.api.client.registry.category.CategoryRegistry;
import me.shedaniel.rei.api.client.registry.display.DisplayRegistry;
import me.shedaniel.rei.api.common.category.CategoryIdentifier;
import me.shedaniel.rei.api.common.util.EntryStacks;
import me.shedaniel.rei.plugin.common.displays.DefaultInformationDisplay;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.text.TranslatableText;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.extractinatortweaks.ExtractinatorTweaks;

import java.util.Iterator;

@Environment(EnvType.CLIENT)
@Mixin(TheExtractinatorClientPlugin.class)
public class TheExtractinatorClientPluginMixin {
    @Inject(method = "registerCategories(Lme/shedaniel/rei/api/client/registry/category/CategoryRegistry;)V", at = @At("HEAD"), cancellable = true, remap = false)
    public void inject(CategoryRegistry registry, CallbackInfo ci){
        registry.add(new ExtractinatorCategory());
        registry.addWorkstations(CategoryIdentifier.of(new ModIdentifier("extractinator")), EntryStacks.of(ExtractinatorTweaks.EXTRACTINATOR_BLOCK));
        ci.cancel();
    }

    @Inject(method = "registerDisplays(Lme/shedaniel/rei/api/client/registry/display/DisplayRegistry;)V", at = @At("HEAD"), cancellable = true, remap = false)
    public void inject(DisplayRegistry registry, CallbackInfo ci){

        for (SupportedBlock block : TheExtractinatorClient.supportedBlocks) {
            String namespace = block.id.getNamespace();
            if (ModUtils.modLoaded(namespace)) {
                registry.add(new ExtractinatorDisplay(block));
            }
        }

        if (ModUtils.modLoaded("subterrestrial")) {
            DefaultInformationDisplay info = DefaultInformationDisplay.createFromEntry(EntryStacks.of(ExtractinatorTweaks.EXTRACTINATOR_BLOCK), new TranslatableText("the_extractinator.rei.extractinator.info.title"));
            info.lines(new TranslatableText("the_extractinator.rei.extractinator.info.body"));
            registry.add(info);
        }

        ci.cancel();
    }
}
