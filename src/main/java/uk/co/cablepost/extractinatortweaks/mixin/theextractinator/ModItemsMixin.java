package uk.co.cablepost.extractinatortweaks.mixin.theextractinator;

import com.github.alexnijjar.the_extractinator.registry.ModBlocks;
import com.github.alexnijjar.the_extractinator.registry.ModItems;
import com.github.alexnijjar.the_extractinator.util.ModIdentifier;
import com.github.alexnijjar.the_extractinator.util.ModUtils;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import uk.co.cablepost.extractinatortweaks.ExtractinatorTweaks;

import java.util.List;

@Mixin(ModItems.class)
public class ModItemsMixin {
    @Inject(method = "register()V", at = @At("HEAD"), cancellable = true, remap = false)
    private static void injected(CallbackInfo ci) {
        ModItems.extractinatorItem = Registry.register(Registry.ITEM, new ModIdentifier("extractinator"), new BlockItem(ExtractinatorTweaks.EXTRACTINATOR_BLOCK, (new FabricItemSettings()).group(ItemGroup.DECORATIONS)) {
            public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
                if (!ModUtils.modLoaded("roughlyenoughitems") && ModUtils.modLoaded("subterrestrial")) {
                    tooltip.add(new TranslatableText("item.the_extractinator.extractinator.tooltip"));
                }

            }
        });
        Registry.register(Registry.ITEM, new ModIdentifier("silt"), new BlockItem(ModBlocks.SILT, (new FabricItemSettings()).group(ItemGroup.BUILDING_BLOCKS)) {
            public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
                tooltip.add(new TranslatableText("item.the_extractinator.silt.tooltip"));
            }
        });
        Registry.register(Registry.ITEM, new ModIdentifier("slush"), new BlockItem(ModBlocks.SLUSH, (new FabricItemSettings()).group(ItemGroup.BUILDING_BLOCKS)) {
            public void appendTooltip(ItemStack stack, World world, List<Text> tooltip, TooltipContext context) {
                tooltip.add(new TranslatableText("item.the_extractinator.silt.tooltip"));
            }
        });

        ci.cancel();
    }
}
