package uk.co.cablepost.extractinatortweaks.mixin.theextractinator;

import com.github.alexnijjar.the_extractinator.TheExtractinator;
import com.github.alexnijjar.the_extractinator.compat.rei.util.Tier;
import com.github.alexnijjar.the_extractinator.data.*;
import com.github.alexnijjar.the_extractinator.util.ModIdentifier;
import com.github.alexnijjar.the_extractinator.util.ModUtils;
import com.google.gson.JsonObject;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.resource.SimpleSynchronousResourceReloadListener;
import net.minecraft.resource.Resource;
import net.minecraft.resource.ResourceManager;
import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.io.InputStreamReader;
import java.util.*;

@Mixin(ResourceData.class)
public class ResourceDataMixin {
    @Inject(method = "register()V", at = @At("HEAD"), remap = false, cancellable = true)
    private static void inject(CallbackInfo ci){
        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(new SimpleSynchronousResourceReloadListener() {
            public Identifier getFabricId() {
                return new ModIdentifier("output");
            }

            public void reload(ResourceManager manager) {
                List<SupportedBlock> supportedBlocks = new ArrayList<>();
                List<LootTable> lootTables = new ArrayList<>();
                Iterator<Identifier> var4 = manager.findResources("output/supported_blocks", (path) -> path.endsWith(".json")).iterator();

                Map<String, Integer> addedSupportedBlocksIndexes = new HashMap<>();
                Map<String, Integer> addedLootTablesIndexes = new HashMap<>();

                Identifier id;
                Resource resource;
                InputStreamReader reader;
                JsonObject jsonObject;
                while(var4.hasNext()) {
                    id = var4.next();

                    try {

                        for (Resource value : manager.getAllResources(id)) {
                            resource = value;
                            reader = new InputStreamReader(resource.getInputStream());
                            jsonObject = JsonHelper.deserialize(ResourceData.GSON, reader, JsonObject.class);
                            if (jsonObject != null) {
                                SupportedBlock supportedBlock = SupportedBlocksParser.parse(jsonObject);
                                String path = id.getPath();
                                if(!addedSupportedBlocksIndexes.containsKey(path)) {
                                    supportedBlocks.add(supportedBlock);
                                    addedSupportedBlocksIndexes.put(path, supportedBlocks.size() - 1);
                                }else{
                                    supportedBlocks.set(addedSupportedBlocksIndexes.get(path), supportedBlock);
                                }
                            }
                        }
                    } catch (Exception var15) {
                        TheExtractinator.LOGGER.error("Failed to load The Extractinator supported blocks from \"" + id.toString() + "\"", var15);
                        var15.printStackTrace();
                    }
                }

                var4 = manager.findResources("output/tiers", (path) -> path.endsWith(".json")).iterator();

                while(var4.hasNext()) {
                    id = var4.next();

                    try {

                        for (Resource value : manager.getAllResources(id)) {
                            resource = value;
                            reader = new InputStreamReader(resource.getInputStream());
                            jsonObject = JsonHelper.deserialize(ResourceData.GSON, reader, JsonObject.class);
                            if (jsonObject != null) {
                                List<LootSlot> loot = LootTableParser.parse(jsonObject);
                                String path = id.getPath();
                                Tier tier = ModUtils.stringToTier(path);
                                String mod = path.split("/")[2].split("/")[0];
                                LootTable lootTable = new LootTable(mod, tier, loot);

                                if(!addedLootTablesIndexes.containsKey(path)) {
                                    lootTables.add(lootTable);
                                    addedLootTablesIndexes.put(path, lootTables.size() - 1);
                                }else{
                                    lootTables.set(addedLootTablesIndexes.get(path), lootTable);
                                }
                            }
                        }
                    } catch (Exception var14) {
                        TheExtractinator.LOGGER.error("Failed to load The Extractinator tiers from \"" + id.toString() + "\"", var14);
                        var14.printStackTrace();
                    }
                }

                TheExtractinator.supportedBlocks = supportedBlocks;
                TheExtractinator.lootTables = lootTables;
            }
        });

        ci.cancel();
    }
}
