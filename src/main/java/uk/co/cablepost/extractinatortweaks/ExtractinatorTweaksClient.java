package uk.co.cablepost.extractinatortweaks;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.screen.ingame.HandledScreens;
import uk.co.cablepost.extractinatortweaks.screen.ExtractinatorScreen;
import uk.co.cablepost.extractinatortweaks.screen.ExtractinatorScreenHandler;

@Environment(EnvType.CLIENT)
public class ExtractinatorTweaksClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        HandledScreens.register(ExtractinatorTweaks.EXTRACTINATOR_SCREEN_HANDLER, ExtractinatorScreen::new);
    }
}
