package uk.co.cablepost.extractinatortweaks.block;

import com.github.alexnijjar.the_extractinator.TheExtractinator;
import com.github.alexnijjar.the_extractinator.blocks.entity.ExtractinatorInventory;
import com.github.alexnijjar.the_extractinator.registry.ModStats;
import com.github.alexnijjar.the_extractinator.util.BlockUtils;
import com.github.alexnijjar.the_extractinator.util.LootUtils;
import net.fabricmc.fabric.api.networking.v1.PlayerLookup;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.extractinatortweaks.ExtractinatorTweaks;
import uk.co.cablepost.extractinatortweaks.screen.ExtractinatorScreenHandler;

import java.util.Iterator;
import java.util.List;

public class TweakedExtractinatorBlockEntity extends BlockEntity implements ExtractinatorInventory, SidedInventory, NamedScreenHandlerFactory {
    public static final int INVENTORY_SIZE = 28;
    private final DefaultedList<ItemStack> inventory;
    private int transferCooldown;
    private int status = STATUS_UNSET;
    private int remainingUsages;

    public static int STATUS_UNSET = -1;
    public static int STATUS_NORMAL = 0;
    public static int STATUS_BLOCKED_INVALID_INPUT_ABOVE = 1;
    public static int STATUS_BLOCKED_INVALID_INPUT = 2;
    public static int STATUS_OUTPUT_FULL = 3;

    public TweakedExtractinatorBlockEntity(BlockPos pos, BlockState state) {
        super(ExtractinatorTweaks.EXTRACTINATOR_BLOCK_ENTITY, pos, state);
        this.inventory = DefaultedList.ofSize(INVENTORY_SIZE, ItemStack.EMPTY);
        int maximumUsages = TheExtractinator.CONFIG.extractinatorConfig.maximumUsages;
        this.remainingUsages = maximumUsages == 0 ? -1 : maximumUsages;
    }

    public static void serverTick(World world, BlockPos pos, BlockState state, TweakedExtractinatorBlockEntity blockEntity) {
        blockEntity.status = STATUS_NORMAL;

        if (blockEntity.remainingUsages == 0 && TheExtractinator.CONFIG.extractinatorConfig.maximumUsages != 0) {
            world.breakBlock(pos, false);
            world.playSound(null, pos, SoundEvents.BLOCK_ANVIL_DESTROY, SoundCategory.BLOCKS, 1.0F, 1.0F);
        }

        BlockState aboveBlock = world.getBlockState(pos.up());
        ItemStack input = blockEntity.inventory.get(0);
        boolean slotAvailable = false;

        for(int i = 1; i < blockEntity.inventory.size(); ++i) {
            if (blockEntity.inventory.get(i).isEmpty()) {
                slotAvailable = true;
                break;
            }
        }

        if (aboveBlock.isAir() && BlockUtils.inputSupported(input.getItem()) || BlockUtils.inputSupported(aboveBlock.getBlock().asItem())) {
            --blockEntity.transferCooldown;
        }
        else{
            blockEntity.transferCooldown = TheExtractinator.CONFIG.extractinatorConfig.inputCooldown;

            if (!aboveBlock.isAir() && !BlockUtils.inputSupported(aboveBlock.getBlock().asItem())) {
                blockEntity.status = STATUS_BLOCKED_INVALID_INPUT_ABOVE;
            } else if (!input.isEmpty() && !BlockUtils.inputSupported(input.getItem())) {
                blockEntity.status = STATUS_BLOCKED_INVALID_INPUT;
            }
        }

        if(blockEntity.needsCooldown()) {
            return;
        }

        if (!slotAvailable) {
            blockEntity.status = STATUS_OUTPUT_FULL;
            return;
        }

        if (aboveBlock.isAir() && BlockUtils.inputSupported(input.getItem())) {
            Block inputBlock = Block.getBlockFromItem(input.getItem());
            input.decrement(1);
            BlockUtils.placeBlockSilently(world, pos.up(), inputBlock);
            blockEntity.setCooldown(TheExtractinator.CONFIG.extractinatorConfig.inputCooldown);
            aboveBlock = world.getBlockState(pos.up());
            markDirty(world, pos, state);
        }

        if (BlockUtils.inputSupported(aboveBlock.getBlock().asItem())) {
            world.breakBlock(pos.up(), false);
            if (blockEntity.remainingUsages > 0) {
                --blockEntity.remainingUsages;
            }

            List<ItemStack> items = LootUtils.extractMaterials(aboveBlock, world.random);
            Iterator var8 = PlayerLookup.around((ServerWorld) world, pos, 32.0).iterator();

            while (var8.hasNext()) {
                ServerPlayerEntity player = (ServerPlayerEntity) var8.next();
                player.increaseStat(ModStats.BLOCKS_EXTRACTINATED, 1);
            }

            var8 = items.iterator();

            int i;
            ItemStack stack;

            label107:
            while (true) {
                while (true) {
                    if (!var8.hasNext()) {
                        break label107;
                    }

                    ItemStack item = (ItemStack) var8.next();

                    for (i = 1; i < blockEntity.inventory.size(); ++i) {
                        stack = blockEntity.inventory.get(i);
                        if (stack.isEmpty() || stack.getItem().equals(item.getItem()) && stack.getCount() < stack.getMaxCount()) {
                            blockEntity.setStack(i, new ItemStack(item.getItem(), item.getCount() + stack.getCount()));
                            break;
                        }
                    }
                }
            }

            markDirty(world, pos, state);
        }
    }

    public void setCooldown(int cooldown) {
        this.transferCooldown = cooldown;
    }

    private boolean needsCooldown() {
        return this.transferCooldown > 0;
    }

    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        if (nbt.contains("transferCooldown")) {
            this.transferCooldown = nbt.getInt("transferCooldown");
        }

        if (nbt.contains("remainingUsages")) {
            this.remainingUsages = nbt.getInt("remainingUsages");
        }

        Inventories.readNbt(nbt, this.inventory);
    }

    public void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);
        nbt.putInt("transferCooldown", this.transferCooldown);
        nbt.putInt("remainingUsages", this.remainingUsages);
        Inventories.writeNbt(nbt, this.inventory);
    }

    public static int PROPERTY_DELEGATE_SIZE = 2;

    public static int PROPERTY_DELEGATE_TRANSFER_COOLDOWN = 0;
    public static int PROPERTY_DELEGATE_STATUS = 1;

    protected final PropertyDelegate propertyDelegate = new PropertyDelegate(){
        @Override
        public int get(int index) {
            if (index == 0) {
                return transferCooldown;
            }
            if (index == 1) {
                return status;
            }
            return 0;
        }

        @Override
        public void set(int index, int value) {
        }

        @Override
        public int size() {
            return PROPERTY_DELEGATE_SIZE;
        }
    };

    public int[] getAvailableSlots(Direction side) {
        if(side != Direction.DOWN){
            return new int[]{0};
        }

        int[] result = new int[this.getItems().size() - 1];

        for(int i = 0; i < result.length; result[i] = i++) {
            result[i] = i + 1;
        }

        return result;
    }

    public boolean canInsert(int slot, ItemStack stack, Direction dir) {
        return BlockUtils.inputSupported(stack.getItem()) && slot == 0;
    }

    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return slot != 0;
    }

    public DefaultedList<ItemStack> getItems() {
        return this.inventory;
    }

    @Override
    public Text getDisplayName() {
        return new TranslatableText(getCachedState().getBlock().getTranslationKey());
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new ExtractinatorScreenHandler(syncId, inv, this, propertyDelegate);
    }
}
