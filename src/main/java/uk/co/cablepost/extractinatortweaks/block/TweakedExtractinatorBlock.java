package uk.co.cablepost.extractinatortweaks.block;

import com.github.alexnijjar.the_extractinator.blocks.ExtractinatorBlock;
import com.github.alexnijjar.the_extractinator.blocks.entity.ExtractinatorBlockEntity;
import com.github.alexnijjar.the_extractinator.blocks.voxel.ExtractinatorBlockVoxel;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.function.BooleanBiFunction;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import uk.co.cablepost.extractinatortweaks.ExtractinatorTweaks;

public class TweakedExtractinatorBlock extends ExtractinatorBlock {
    public TweakedExtractinatorBlock(Settings settings) {
        super(settings);
    }

    public static VoxelShape baseShape = VoxelShapes.combineAndSimplify(
        Block.createCuboidShape(0, 0, 0, 16, 8, 16),
        Block.createCuboidShape(1, 8, 1, 15, 11, 15),
        BooleanBiFunction.OR
    );

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        if (!world.isClient) {
            NamedScreenHandlerFactory screenHandlerFactory = state.createScreenHandlerFactory(world, pos);
            if (screenHandlerFactory != null) {
                player.openHandledScreen(screenHandlerFactory);
            }
        }

        return ActionResult.SUCCESS;
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new TweakedExtractinatorBlockEntity(pos, state);
    }

    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ? null : checkType(type, ExtractinatorTweaks.EXTRACTINATOR_BLOCK_ENTITY, TweakedExtractinatorBlockEntity::serverTick);
    }

    public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean moved) {
        if (!state.isOf(newState.getBlock())) {
            BlockEntity blockEntity = world.getBlockEntity(pos);
            if (blockEntity instanceof TweakedExtractinatorBlockEntity tweakedExtractinatorBlockEntity) {
                ItemScatterer.spawn(world, pos, tweakedExtractinatorBlockEntity);
                world.updateComparators(pos, this);
            }

            super.onStateReplaced(state, world, pos, newState, moved);
        }

    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {

        VoxelShape addedShape = switch (state.get(FACING)) {
            case NORTH -> Block.createCuboidShape(8, 11, 4, 14, 16, 12);
            case EAST -> Block.createCuboidShape(4, 11, 8, 12, 16, 14);
            case SOUTH -> Block.createCuboidShape(2, 11, 4, 8, 16, 12);
            case WEST -> Block.createCuboidShape(4, 11, 2, 12, 16, 8);
            default -> null;
        };

        return VoxelShapes.combineAndSimplify(
                baseShape,
                addedShape,
                BooleanBiFunction.OR
        );
    }

    @Override
    public void onEntityLand(BlockView world, Entity entity) {
    }
}