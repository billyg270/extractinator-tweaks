package uk.co.cablepost.extractinatortweaks;

import com.github.alexnijjar.the_extractinator.blocks.ExtractinatorBlock;
import com.github.alexnijjar.the_extractinator.client.TheExtractinatorClient;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.model.BakedModelManagerHelper;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedModelManager;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.screen.PlayerScreenHandler;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3f;
import net.minecraft.world.World;
import uk.co.cablepost.extractinatortweaks.block.TweakedExtractinatorBlockEntity;

import java.util.List;

@Environment(EnvType.CLIENT)
public class TweakedExtractinatorBlockEntityRenderer implements BlockEntityRenderer<TweakedExtractinatorBlockEntity> {

    public static double[] sineApprox = null;

    public int occTime = 0;

    public double getSineApprox(){
        if(sineApprox == null){
            sineApprox = new double[36];
            for(int i = 0; i < sineApprox.length; i++){
                sineApprox[i] = Math.sin(i * 0.174533d);
            }
        }
        return sineApprox[Math.round(occTime/2000f) % sineApprox.length];
    }

    public TweakedExtractinatorBlockEntityRenderer() {
    }

    public void render(TweakedExtractinatorBlockEntity entity, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {
        MinecraftClient minecraftClient = MinecraftClient.getInstance();
        BakedModelManager manager = minecraftClient.getBakedModelManager();
        BakedModel grinderModel = BakedModelManagerHelper.getModel(manager, TheExtractinatorClient.GRINDER_PATH);
        World world = entity.getWorld();
        if (world != null) {
            Direction direction = entity.getCachedState().get(ExtractinatorBlock.FACING);
            render(grinderModel, direction, world, matrices, vertexConsumers, light, overlay, tickDelta);
        }
    }

    void render(BakedModel model, Direction direction, World world, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay, float tickDelta) {
        matrices.multiply(direction.getRotationQuaternion());
        matrices.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(-90.0F));
        matrices.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion(180.0F));
        switch (direction) {
            case EAST -> matrices.translate(0.0, 0.0, -1.0);
            case SOUTH -> matrices.translate(-1.0, 0.0, -1.0);
            case WEST -> matrices.translate(-1.0, 0.0, 0.0);
        }

        render(model, world, matrices, vertexConsumers, light, overlay, tickDelta);
    }

    void render(BakedModel model, World world, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay, float tickDelta) {
        matrices.push();

        //double sine = Math.sin((double)((float)world.getTime() + tickDelta) / 1.5);
        //occTime += Math.round(tickDelta * 1000);
        occTime = Math.round((world.getTime() + tickDelta) * 10000);
        double sine = getSineApprox();
        matrices.translate(0.0, sine * 0.09, 0.0);
        matrices.translate(0.0, 0.1, 0.0);

        VertexConsumer vertexConsumer = vertexConsumers.getBuffer(RenderLayer.getEntitySolid(PlayerScreenHandler.BLOCK_ATLAS_TEXTURE));
        List<BakedQuad> quads = model.getQuads(null, null, world.random);
        MatrixStack.Entry entry = matrices.peek();

        for (BakedQuad quad : quads) {
            vertexConsumer.quad(entry, quad, 1.0F, 1.0F, 1.0F, light, overlay);
        }

        matrices.pop();
    }
}
